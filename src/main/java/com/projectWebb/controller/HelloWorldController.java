package com.projectWebb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloWorldController {
	
	/*String mensaje = "Bienbenido a Spring MVC";*/
	
	@RequestMapping("/holaMundo")
	public String execute(){
		System.out.println("Executando a logica com Spring MVC");
	    return "ok";
	}
	
}
